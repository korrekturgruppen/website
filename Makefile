all: develop

develop:
	@./node_modules/.bin/grunt develop

production:
	@./node_modules/.bin/grunt production

watch:
	@./node_modules/.bin/grunt watch

clean:
	@./node_modules/.bin/grunt clean

jshint:
	@./node_modules/.bin/jshint js/

.PHONY: develop production watch jshint clean
