# The Grammatic.al website

You need [http://nodejs.org/](node.js) installed.

## Build js and css

Fire up a terminal and cd to the location of the website.
Install the needed node modules

```
$ npm install
```

Compile js and css files in development mode.

```
$ make develop
```

Auto compile on file changes

```
$ make watch
```

Remove compiled files.

```
$ make clean
```

Run `jshint` on the Javascript code

```
$ make jshint
```
