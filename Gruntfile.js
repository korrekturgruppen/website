module.exports = function (grunt) {

  grunt.loadNpmTasks('grunt-browserify');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-sass');

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    browserify: {
      prod: {
        files: {
          'js/grammatical.js': ['js/main.js']
        },
        options: {
          transform: ['browserify-shim', 'uglifyify']
        }
      },
      dev: {
        files: {
          'js/grammatical.js': ['js/main.js']
        },
        options: {
          transform: ['browserify-shim']
        }
      }
    },
    clean: {
      build: ['js/grammatical.js'],
      all: ['js/grammatical.js', 'node_modules', 'bower_components']
    },
    sass: {
      dev: {
        files: {
          'stylesheets/main.css': 'sass/main.scss',
          'stylesheets/result.css': 'sass/result.scss'
        }
      },
      prod: {
        options: {
          outputStyle: 'compressed'
        },
        files: {
          'stylesheets/main.css': 'sass/main.scss',
          'stylesheets/result.css': 'sass/result.scss'
        }
      }
    },
    watch: {
      scripts: {
        files: ['**/*.js', '**/*.scss', '**/*.html', '**/*.json'],
        tasks: ['develop']
      },
      options: {
        spawn: false
      }
    }
  });

  grunt.registerTask('production', ['clean:build', 'browserify:prod', 'sass:prod']);
  grunt.registerTask('develop', ['browserify:dev', 'sass:dev']);
};
