<?php
  function escSuggestion($str){
    return str_replace('"', '\\"', $str);
  }

?>
<!DOCTYPE html>
<html>
  <head>
    <title>Grammatic.al</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta id="mvp" name="viewport" content="width=device-width, user-scalable=no">
    
    <link rel="icon" type="image/png" href="../img/logo/gr.png">
    <link rel="icon" type="image/x-icon" href="../favicon.ico">

    <link href="../stylesheets/reset.css" rel="stylesheet" type="text/css" media="all" />
    <link href="../stylesheets/result.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/spellchecker.css" rel="stylesheet" type="text/css" media="all" />
  </head>

  <body>
    <div id="content">

      <div id="menu">
        <a href="http://grammatic.al" id="menu_logo" class="menu_link grammatical-font">
          <span></span>
        </a>      
      </div>

      <div id="resultwrapper" class="angel">
        <div id="result" onclick="clickDiv(this)" lang="klingon" spellcheck="false" maxlength="1000"><?php
          $MAX = 1000;

          if(isset($_GET["text"])){
            $text = $_GET["text"];
          } else {
            $text = "";
          }

          $text = substr($text, 0, $MAX);

          $left = $MAX - strlen($text);

          // get tense
          $sentence = preg_split("/[^\S\n]+/", $text);

          $postData = array(
            'words' => $sentence
          );

          $url = 'https://api.grammatic.al/api/v1/words/spellchecks';
          //$url = 'http://localhost:8080/api/v1/words/spellchecks';

          $ch = curl_init($url);
          curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
              //'Authorization: '.$authToken,
              'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($postData)
          ));

          $response = curl_exec( $ch );

          // print_r($response);
          // die();

          // Check for errors
          if(!$response){
            die(curl_error($ch));
          }

          //close connection
          curl_close($ch);
          
          $data = json_decode($response);

          if(!isset($data)){
            echo "<span class='no_err'>Server error</span>";
          }
          else if($data->status == "error"){
            echo "<span class='spell_err gr_err' related=\"" . $data->message . "\">" .  $text . "</span>";
          } else {

            $data = $data->data;

            for($i = 0; $i < sizeof($data); $i++){

              $sentence = $data[$i];   
              for($k = 0; $k < sizeof($sentence); $k++){
                $w = $sentence[$k];
                $span = "<span class=";

                $related = [];

                if (!$w->spelled_correct) {
                  for ($j = 0; $j < sizeof($w->related_to); $j++) {
                    $related_word = str_replace(" ", "_", $w->related_to[$j]->word); // ' ' => '_'
                    array_push($related, $related_word);
                  }
                }

                if(!$w->spelled_correct){
                  $span .= "'spell_err gr_err' ";
                  $span .= "related=\"". escSuggestion(implode($related, " ")) . "\">";
                }
                elseif(isset($w->grammar_err)){
                  $span .= "'grammar_err gr_err' ";  
                  $span .= "related=\"". escSuggestion($w->grammar_err->suggestion) . "\">";
                }
                else {
                  $span .= "'no_err'>";
                }
                
                $input = $w->input;
                $html = htmlspecialchars($input);
                $html = str_replace("\n","",$html);
                $span .= $html;
                $span .= "</span> ";

                echo $span;

                for($m = 1; $m < strlen($input); $m++){
                  if($input[$m] . $input[$m-1] === "\n\n"){
                    echo "<br />";
                  }
                }
              }
            }
          }
        ?> </div>
      <div id="counterwrapper"><span id="counter"><?php echo $left;?></span> tegn tilbage</div>
      <div id="submit" class="button">KORREKTUR</div>
      </div>
    </div>

    <link href='http://fonts.googleapis.com/css?family=Nunito:400,300,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Abel' rel='stylesheet' type='text/css'>
    <link href="../stylesheets/font.css" rel="stylesheet" type="text/css" media="all" />

    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="../js/result.js"></script>

    <script type="text/javascript">

      var popup_holder = $('<div id="popup" style="position:absolute; z-index: 999999;"></div>');

      closePopups();
      $(popup_container).remove();
      $("body").append(popup_holder);
      popup_container = popup_holder;

      $(".spell_err, .grammar_err").mouseenter(function(){
        initPopup(this);
      });

      url = "http://grammatic.al/result/"
      //url = "http://localhost/grammatical/website/result/";

      $("#submit").click(function(){
        var text = toText(document.getElementById("result"));
        text = encodeURIComponent(text);
        
        window.location.href = url + "?text=" + text;
      });

      function clickDiv(div){
        div.contentEditable='true';
        div.focus();
      }

      document.getElementById("result").addEventListener("input", function() {
        counter();
      }, false);
      function counter(){
        var div = document.getElementById("result");
        var max = <?php echo $MAX; ?>;
        var text = $("#result").text();
        var left = max - text.length;

        if(left < 0){
          left = 0;
          text = text.substring(0, max);
        }

        /*
        $("#result").text(text);
        placeCaretAtEnd(div)
        */
        $("#counter").text(left);

        $(".gr_err").each(function(){
          $(this).removeClass();
          $(this).addClass("no_err");
        });
      }
      
    </script>
    
  </body>
</html>
