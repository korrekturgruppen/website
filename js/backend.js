var $ = require('jquery');

module.exports = {
  url: "https://api.grammatic.al/api/v1/words/",
  //url: "http://localhost:8080/api/v1/words/",

  // post one word to backend api
  postWord: function (word, callback) {
    var type = "POST";
    var data = {
      word: word
    };

    var success = function(response){
      callback(response);
    };

    $.ajax({
      type: type,
      url: this.url + "spellcheck",
      data: data,
      success: success
    });
  },

// post array of words to backend api
  postWords: function (words, callback) {
    if (words.length === 0) {
      var empty = {};
      empty.data = {};
      callback(empty);
      return;
    } else {

      var type = "POST";
      var data = {
        words: words
      };

      var success = function (response) {
        callback(response);
      };

      $.ajax({
        type: type,
        url: this.url + "spellchecks",
        data: data,
        success: success
      });
    }
  }
};
