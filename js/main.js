var $ = require('jquery');

var menu = require('./menu');
var signup = require('./signup');
var spellcheck = require('./spellcheck');
var login = require('./login');

$(document).ready(function () {
  // initialize UI
  menu.init();
  signup.init();
  spellcheck.init();
  login.init();
});
