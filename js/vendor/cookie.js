var CookieInformerBooklet = CookieInformerBooklet || {};
var CookieInformerBooklet = {
    config: {
        title: "Accept af cookies",
        text: 'Websitet anvender cookies til at huske dine indstillinger og statistik. Denne information deles med tredjepart. <a href="LINK-TIL-PRIVATLIVSPOLITIK" target="_blank">Læs mere >> </a>',
        buttonText: "Cookies"
    },
    init: function (e) {
        jQuery.extend(this.config, e);
        if (this.getCookie() == "hidden") {
            return
        }
        this.createElements();
        this.bindEvents()
    },
    createElements: function () {
        this.$container = jQuery("<div />", {
            id: "cookieInformerBooklet"
        }).css({
            bottom: "0",
            height: "1px",
            left: "0",
            overflow: "visible",
            position: "fixed",
            right: "0",
            zIndex: "11001"
        });
        this.$button = jQuery("<div />", {
            text: "Ok",
            "class": "button"
        }).css({
            bottom: "10px",
            right: "10px",
            cursor: "pointer",
            display: "block",
            width: "50px",
            height: "30px",
            lineHeight: "30px",
            position: "absolute",
            zIndex: "1000"
        });
        this.$content = jQuery("<div />", {
            "class": "cookieContent"
        }).css({
            backgroundColor: "#FFFFFF",
            borderColor: "#000",
            borderRadius: "0 0 0 0",
            borderStyle: "solid solid none",
            borderWidth: "1px 0px medium",
            bottom: "0",

            color: "#000",
            display: "block",
            left: "0px",
            opacity: "0.9",
            padding: "10px 65px 10px 10px",

            position: "absolute",
            right: "0px",
            zIndex: "999",
            height: "auto",
            minHeight: "30px",
        });
        this.$contentText = jQuery("<p />").css({
            minHeight: "25px",
            paddingLeft: "30px",
            background: "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyRpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoTWFjaW50b3NoKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo3Nzc2NzUxRTVDNTYxMUUzODM4Q0JFMTI0NjQ5RUUzMyIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo3Nzc2NzUxRjVDNTYxMUUzODM4Q0JFMTI0NjQ5RUUzMyI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjFCRkE1Mzg5NUMwQzExRTM4MzhDQkUxMjQ2NDlFRTMzIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjFCRkE1MzhBNUMwQzExRTM4MzhDQkUxMjQ2NDlFRTMzIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+iUM6/QAABPdJREFUeNrcVm1MU2cUfm6/C5EuYa7AgltkGQPn1xJhmMWsZqCiRnSCEY2gDBuHkjmwcwsbzgXZH9zEZIvaGWSamLhMnaKMuRDxzxjDIB1sIh9WoIAIFmjL7e3t7c7bAoqgJibbj9303Lf3nPOe555znvd9L+fz+fBvXzL8B5eC3Vwu1xOdlEolent7tUajsVilUi0URbF65cqVRZGRkR76/8S5KSkpAZCGhoYnOmq1WhQfOPC14W3D9tQNqSg9VLqkpqaGT0pK+nJkZAQcxz09k9ra2sc6qNVqdHR0xEuiuG2raQ9aKWlTUTHSEpfm9/T0nEpISOjkef7pIHq9flqjXC7H0NAQrlRV7S89alZYHMDNAQGKMDWMO3NDz/1wZl96enrW08rNMXbdunVrWuOMGTNQVla2/nZb2xnTkWP4udMDt+RDsEKGxDA59mxYJ74+b/5bsXPm1Lrd7mljbN68OZBJS0vLVNrJZKCJ2ooLP+0/XHYStfd8sHsksPKPur3406VATv5HClPuzv1qjWYFZS09bjn4QaxW67TNvnzp0vbly1fEiC/Pxs0OHl6WOgkL1dDvxrq4N/HGwoVJdrt9TW5u7llGgseCUPMmKTUaDSwWi97WecdU+I0ZFTYvHISw4yUVdAoOQ6IP31oFVN+VkJW/F6bszCKLwVBF85ySJE2KFR0dHQBh6+BRkPNnzxZsyd4e0arWob2XB7UBs7QyKCkVHd14itU0KGBOVBQSk5bFmM1mY3Jy8sFHKW0wGKaWiwEQEWKH7fez4tO24HiXAIcoQUkgZ2wCtHIOo16fX0c/VHaJSDfuQtXa5Hxi4snY2Ni7giBMLRdr8jhlnU4nTn1fvi//08+111xKWJ0uyOjFOAp4yuaBNLYXaUjJetMyLKAxVIeNGVvDr1X/+klqauoHDodjKoVHR0cnml1efsJwpfLylexjp2Ulf/PUC2IURWPlKnlNg+dVHO4JPuSRjWXCgGaq5dg9W46ijavchsRlcTExsY2M0qxsq1evDmTS1tbmV3i9XmWZ2VxccPCw7FyvhB63CDnpGTNVJHo1h1ACoYrBRSUTCISVv93pQeWwGtt25akPFX/xmV4ftt7j8UwuV3NzM4KCglBx8UJ6XFxc/PCrC3DV4vQ3V05pUDxoSISxZcBGJykDdvjtlZ0uxC9OROSL36212WyGnJyc6nFK+5vR3t6Ouro6XXPjjYJV7+fhRIcHvRTJQUFGvA9kfK2x8WE987vjlnDaJmHT7r2yXy5XFPX19SnGs/GDpKWl4a+mppw169NeqQ+OQM0Aj2GKNEKveJ9kkIStDQkBFDay58ExO/Oz03ZT1eNEf9QCzJ07L6G09NCm7u7uByD19fVRzqH7e+ZvfA/HWkf9k0do0qAUmOwgGSBQ9dgRx0b27BizMz8niY2Aj3YIWLHjQ1z/vfbj21ZryATIhfPnct/NyHruPB+CJqKkf/8QJws7m67bJdwY9vpHUZzqw+bV9PP4I2QWVq1ZG33cbM6cABno71+0aMlSdLMdW6UAp5JPFqIop5RjucWNBXW8f2TPfv1DfmBC+i46XhYvfQcqpSJhgl0vhEdc/bHsaIIp04iMkGB4pWf/uFAR3ULdVpw+cRxh4RG/TSxGOn41hYWFBZLALxMFIWRso32mi+LJ1EHBPTPDwi+WlJR8pdPpRO5/80n0jwADALKlerspAOXaAAAAAElFTkSuQmCC) center left no-repeat"
        }).html(this.config.text);
        this.$content.append(this.$contentTitle).append(this.$contentText);
        this.$container.appendTo("body").append(this.$button).append(this.$content)
    },
    bindEvents: function () {
        this.$button.click(this.setCookie)
    },
    setCookie: function () {
        var e = "CookieInformerBooklet",
            t = "hidden",
            n = 20 * 365;
        if (n) {
            var r = new Date;
            r.setTime(r.getTime() + n * 24 * 60 * 60 * 1e3);
            var i = "; expires=" + r.toGMTString()
        } else var i = "";
        document.cookie = e + "=" + t + i + "; path=/";
        CookieInformerBooklet.$container.fadeOut(function () {
            jQuery(this).remove()
        })
    },
    getCookie: function () {
        var e = "CookieInformerBooklet";
        if (document.cookie.length > 0) {
            c_start = document.cookie.indexOf(e + "=");
            if (c_start != -1) {
                c_start = c_start + e.length + 1;
                c_end = document.cookie.indexOf(";", c_start);
                if (c_end == -1) {
                    c_end = document.cookie.length
                }
                return unescape(document.cookie.substring(c_start, c_end))
            }
        }
        return ""
    }
};
(function () {
    function r() {
        jQuery(document).ready(function () {
            var e = {
                title: "",
                text: '<p style="padding-top:4px;">Grammatic.al bruger cookies til at huske dine indstillinger og statistik. Ved brug af siden accepteres dette. <a style="color: #0083b4; text-decoration:none;" href="#">Læs mere</a></p>',
                buttonText: "Cookies"
            };
            CookieInformerBooklet.init(e)
        })
    }

    function i(e, t) {
        if ("undefined" === typeof e) {
            throw new Error("compareVersion needs at least one parameter.")
        }
        t = t || $.fn.jquery;
        if (e == t) {
            return 0
        }
        var n = s(e);
        var r = s(t);
        var i = Math.max(n.length, r.length);
        for (var o = 0; o < i; o++) {
            n[o] = n[o] || 0;
            r[o] = r[o] || 0;
            if (n[o] == r[o]) {
                continue
            }
            return n[o] > r[o] ? 1 : 0
        }
        return 0
    }

    function s(e) {
        return $.map(e.split("."), function (e) {
            return parseInt(e, 10)
        })
    }
    var e = "1.5.1";
    if (window.jQuery === undefined || i(e, window.jQuery.fn.jquery)) {
        var t = false;
        var n = document.createElement("script");
        n.src = "http://ajax.googleapis.com/ajax/libs/jquery/" + e + "/jquery.min.js";
        n.onload = n.onreadystatechange = function () {
            if (!t && (!this.readyState || this.readyState == "loaded" || this.readyState == "complete")) {
                t = true;
                r()
            }
        };
        document.getElementsByTagName("head")[0].appendChild(n)
    } else {
        r()
    }
})()