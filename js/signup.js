var $ = require('jquery');

module.exports = {

  init: function () {

    $(".price").click(function(e){
      e.preventDefault();
      $("#signup_form").fadeIn();
    });

    $("#pulse").click(function(e){
      e.preventDefault();
      $("#signup_form").fadeIn();
    });


    $("#close_signup").click(function(e){
      e.preventDefault();

      $("#signup_form").fadeOut();
    });

    // $(".promo").hover(function(e){
    //   $(this).toggleClass("angel");
    // });
  }
};
