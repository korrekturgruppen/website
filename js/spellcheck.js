var $ = require('jquery');
var func = require('./functions');
var backend = require('./backend');

module.exports = {
  popupErrOut: function (span) {
    $(span).children().remove();
  },

  popupErr: function (span) {
    var position = func.getOffsetRect($(span).get(0));
    var popup = $('<div class="grammatical_popup" style="left:' + (position.left) + 'px;"><span></span></div>');

    var related = $(span).attr("related").split(" ");
    for(var i = 0; i < related.length; i++){
      var related_text = related[i].replace(/_/g," ");

      $('<div/>', {
        class: 'relation',
        style: 'width:auto; z-index:999; cursor:pointer; padding:5px;',
        text: related_text
      }).appendTo(popup);
    }
    $(span).append( $(popup) );
  },

  init: function () {
    var popupErr = this.popupErr;
    var popupErrOut = this.popupErrOut;

    max = 500;
    //binding keyup/down events on the contenteditable div
    $('#tryout').keyup(function(e){ check_charcount(max, e); });
    $('#tryout').keydown(function(e){ check_charcount(max, e); });

    function check_charcount(max, e) {
        if (e.which !== 8 && $('#tryout').text().length > max)
        {
            e.preventDefault();
        }
    }

    $("#tryout").focus(function (){
      if ($(this).text().trim() === "Indsæt tekst her"){
        $(this).html("");
      }
    });

    $("#tryout").focusout(function () {
      if($(this).text() === ""){
        $(this).html("Indsæt tekst her");
      }
    });

    $("#trybutton").click(function (e) {
      e.preventDefault();

      var text = $("#tryout").val();

      text = encodeURIComponent(text);
      var url = "http://grammatic.al/result/";
      window.location.href = url + "?text=" + text;
    });
  }
};
