RegExp.escapePattern = function(s) {
  return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
};

RegExp.escapeReplace = function(s) {
  return s.replace(/\$/g, "$$");
};

module.exports = {
  replaceAll: function (str, find, replaceBy) {
    find = RegExp.escapePattern(find);
    replaceBy = RegExp.escapeReplace(replaceBy);
    var regex = new RegExp("(^|\\s)" + find + "(\\s|$)", "g");
    return str.replace(regex, "$1" + replaceBy + "$2");
  },

  // position of dom elemnt x,y
  getOffsetRect: function (elem) {
    // (1)
    var box = elem.getBoundingClientRect();

    var body = document.body;
    var docElem = document.documentElement;

    // (2)
    var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
    var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;

    // (3)
    var clientTop = docElem.clientTop || body.clientTop || 0;
    var clientLeft = docElem.clientLeft || body.clientLeft || 0;

    // (4)
    var top  = box.top +  scrollTop - clientTop;
    var left = box.left + scrollLeft - clientLeft;

    return {
      top: Math.round(top),
      left: Math.round(left),
      height: box.height,
      bottom: box.bottom,
      screenTop: box.top
    };
  }
};
