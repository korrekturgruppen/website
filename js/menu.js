// depends on jquery.scrollto
var $ = require('jquery');

module.exports = {
  init: function () {
    $("#mobile_navigation").click(function () {
      $("#mobile_menu").show();
    });

    $("#close_mobile_menu").click(function () {
      $("#mobile_menu").hide();
    });

    var fixedPosition = 90;
    var menuHeight = 55;

    var scrollFunction = function () {

      var scrollTop = $(window).scrollTop();

      if (scrollTop > fixedPosition) {
        $("#menu").css("position","fixed");
        $("#menu").css("top",0);
        $("#menu_logo").css("top",0);
        $("#menu_logo").show();
      } else {
        $("#menu").css("position","absolute");
        $("#menu").css("top","90px");
        $("#menu").css("position","absolute");
        var rest = fixedPosition - scrollTop;
        $("#menu_logo").css("top",rest+"px");
        $("#menu_logo").hide();
      }

      var pageHeight = $("#content").height();
      var pageNo = (scrollTop+menuHeight) / (pageHeight);
      var white = Math.floor(pageNo) % 2 !== 0;

      if (white) {
        // white page
        $("#menu").css("background-color","#0083b4");
        $("#navigation li a").css("color","#FFFFFF");
        $("#menu_logo span").css("color","#FFFFFF");
      } else {
        // blue page
        $("#menu").css("background-color","#FFFFFF");
        $("#navigation li a").css("color","#0083b4");
        $("#menu_logo span").css("color","#0083b4");

      }
    };

    $(window).scroll(function(){
      scrollFunction();
    });

    $(".menu_link").click(function(e){
      e.preventDefault();

      $("#mobile_menu").hide();
      var hash = $(e.currentTarget).attr("href");
      hash = hash.substring(1);

      $.scrollTo("a[name='"+ hash +"']", 500, {
        axis : 'y',
        offset: { top: 0 },
        onAfter: function() {
          scrollFunction();
        }
      });
    });

    scrollFunction();


    function nextSlide(){
      var active = $(".slide-item.active");
      var index = active.index();
      var parent = $("#slide-items");  
      var new_index = next(index, parent.children().length);
      changeSlide(new_index);
    }

    var timer = setInterval(nextSlide, 7500);

    function prev(index, num_child){
      return index == 0 ? num_child-1 : index-1;;
    }

    function next(index, num_child){
      return index == num_child-1 ? 0 : index+1;
    }

    $(".slide-control").click(function(){
      window.clearInterval(timer);
      var active = $(".slide-item.active");
      var index = active.index();
      var parent = $("#slide-items");  
      
      var new_index;
      if($(this).attr('id') == "prev-slide"){
        new_index = prev(index, parent.children().length);  
      } else {
        new_index = next(index, parent.children().length);  
      }
      changeSlide(new_index);
    });

    function changeSlide(new_index){

      var active = $(".slide-item.active");
      active.removeClass("active");
      var parent = $("#slide-items");  
      var new_active = parent.children().eq(new_index);
      new_active.addClass("active");

      $("#overview .center .current").removeClass("current");
      $("#overview .center").children().eq(new_index).addClass("current");
    }

    $("#overview .center img").click(function(){
      window.clearInterval(timer);
      var index = $(this).index();
      changeSlide(index);
    });




  }
};
