/** SPELLCHECKER **/
var popup_container;

function closePopups() {
  $(popup_container).children().each(function () {
    $(this).remove();
  });
}

function initPopup(span) {
  if (hasSelection()) {
    return;
  }

  var position = getOffsetRect($(span).get(0));
  var popup = $('<div class="grammatical_popup" style="left: ' +
                position.left + 'px;"><span></span></div>');

  var related = $(span).attr("related").split(" ");

  var clickFunction = function () {
    changeWord(this, span);
  };
  var enterFunction = function(){ $(this).css("color","#0083b4"); };
  var leaveFunction = function(){ $(this).css("color","#000"); };

  if(related[0] === "" && related.length === 1){

    $('<div/>', {
        class: 'relation',
        style: 'width:auto; z-index:999; cursor:default; padding:5px;',
        text: "Ingen foreslag"
      }).appendTo(popup);

  } else {

    for(var i = 0; i < related.length; i++){
      var related_text = related[i].replace(/_/g," ");

      $('<div/>', {
        class: 'relation',
        style: 'width:auto; z-index:999; cursor:pointer; padding:5px;',
        text: related_text,
        click: clickFunction,
        mouseenter: enterFunction,
        mouseleave: leaveFunction
      }).appendTo(popup);
    }
  }

  $(popup).mouseleave(function(){
    closePopups();
  });

  closePopups();
  popup_container.append(popup);

  var offset = 425 + ($("#result").height() - 200);//
  $(popup).css("top",(position.top + position.height - offset) + 'px');
}


// triggered when clicking on a related word
// to the mispelled
function changeWord(div, span){
  var word = $(span).text();
  var change_to = $(div).text();
  var related_index = $(div).index();

  $(span).html(change_to);

  closePopups();

  $(span).css("border","0");
  $(span).css("background-image", "none");

  // TODO: should notify, in production
  //notifyBackend(word, change_to, related_index);
  
  $(span).unbind('mouseenter mouseleave');
  $(span).css("cursor","auto");
}










/** HELPER FUNCTIONS **/

function clearTrailingNonChars(str){
  var input = str;
  for(var i = input.length-1; i > 0; i--){
    if(input[i].toUpperCase() !== input[i].toLowerCase()){
      input = input.substring(0,i+1);
      break;
    }
  }
  return input;
}

function clearPrefixNonChars(str){
  var i;
  for(i = 0; i < str.length; i++){
    if(str[i].toUpperCase() === str[i].toLowerCase()){
      continue;
    } else {
      break;
    }
  }
  return str.substring(i,str.length);
}


function hasSelection(){
  var selection = window.getSelection().toString();
  return selection.length !== 0;
}

function uniqueArray(arr){
  var u = {}, a = [];
  for(var i = 0, l = arr.length; i < l; ++i){
      if(u.hasOwnProperty(arr[i])) {
         continue;
      }
      a.push(arr[i]);
      u[arr[i]] = 1;
  }
  return a;
}


function placeCaretAfterNode(node, area) {
  if (typeof window.getSelection != "undefined") {
    var range = document.createRange();
    range.setStartAfter(node);
    range.collapse(true);
    var selection = window.getSelection();
    selection.removeAllRanges();
    selection.addRange(range);

    var element = node.parentNode;
    var doc = element.ownerDocument || element.document;
    var win = doc.defaultView || doc.parentWindow;
    range = win.getSelection().getRangeAt(0);
    var preCaretRange = range.cloneRange();
    preCaretRange.selectNodeContents(element);
    preCaretRange.setEnd(range.endContainer, range.endOffset);
    caretOffset = preCaretRange.toString().length;

    $(area).get(0).setSelectionRange(caretOffset+1,caretOffset+1);

    //$(area).get(0).setSelectionRange
  }
}


function setCursor(node, pos) {
    var node = (typeof node == "string" || node instanceof String) ? document
            .getElementById(node) : node;
    if (!node) {
        return false;
    } else if (node.createTextRange) {
        var textRange = node.createTextRange();
        textRange.collapse(true);
        textRange.moveEnd(pos);
        textRange.moveStart(pos);
        textRange.select();
        return true;
    } else if (node.setSelectionRange) {
        node.setSelectionRange(pos, pos);
        return true;
    }
    return false;
}

function setCursorInDiv(div, pos, text){
  console.log(text);

  var nl = 0;
  var end_pos = pos;
  var counter = 0;

  for(var i = 0; i < pos; i++){
    counter++;

    if(text[i] === "\n"){
      nl++;
      end_pos -= counter;
      counter = 0;
    }
  }

  var range = document.createRange();
  var sel = window.getSelection();
  var fc = div.childNodes[nl];

  if(nl !== 0) {
    fc = fc.firstChild;
  }

  range.setStart(fc, end_pos);

  range.collapse(true);
  sel.removeAllRanges();
  sel.addRange(range);
}


function setCaretInDiv(div, pos){
  var range = document.createRange();
  var sel = window.getSelection();
  range.setStart(div.childNodes[0], 0);
  range.collapse(true);
  sel.removeAllRanges();
  sel.addRange(range);
}

function setSelectionAll(area, setPoint){
  if(area.get(0).tagName === "TEXTAREA"){
    $(area).get(0).setSelectionRange(setPoint, setPoint);
  }
  else{
    console.log("in div");
    var area_text = getTextNl(area);
    setCursorInDiv(area.get(0), setPoint, area_text);
  }
}


function getTextNl(textarea){
  var tag = $(textarea).get(0).tagName;
  var text;

  if(tag === "DIV"){
    text = $(textarea).html();
    text = text.replace(/^<div>((?:(?!<\/div>).)*)<\/div>/,"$1");
    text = text.replace(/<div><br><\/div>/g,"\n");
    text = text.replace(/<div>((?:(?!<\/div>).)*)<\/div>/g,"\n$1");
    text = text.replace(/<div>/g,"");
    text = text.replace(/<\/div>/g,"");
    text = text.replace(/&nbsp;/g," ");
    text = text.replace(/<br>/g,"\n");
  } else {
    text = $(textarea).val();
  }

  return escapeHtml(text);
}


function getText(textarea){
  var tag = $(textarea).get(0).tagName;
  var text;

  if(tag === "DIV"){
    text = $(textarea).text();
  } else {
    text = $(textarea).val();
  }
  return text;
}


// get position of cursor caret inside div
// http://stackoverflow.com/questions/4811822/get-a-ranges-start-and-end-offsets-relative-to-its-parent-container/4812022#4812022
function getCaretCharacterOffsetWithin(element) {
  var caretOffset = 0;
  var doc = element.ownerDocument || element.document;
  var win = doc.defaultView || doc.parentWindow;

  var sel = win.getSelection();
  var range = sel.getRangeAt(0);
  var preCaretRange = range.cloneRange();

  preCaretRange.selectNodeContents(element);
  preCaretRange.setEnd(range.endContainer, range.endOffset);
  // caretOffset = preCaretRange.toString().length;

  sel.removeAllRanges();
  sel.addRange(preCaretRange);
  caretOffset = sel.toString().length;


  console.log(preCaretRange);
  console.log(sel.toString());
  console.log("offset " + caretOffset);

  //sel.removeAllRanges();

  return caretOffset;
}



// whitespace in end of a string
function backtrailingWhitespace(str){
  var i;

  for(i = str.length-1; i >= 0; i--){
    if(!/\s/.test(str[i])){
      i++;
      break;
    }
  }

  return str.substring(i);
}

// position of dom elemnt x,y
function getOffsetRect(elem) {
  // (1)
  var box = elem.getBoundingClientRect();

  var body = document.body;
  var docElem = document.documentElement;

  // (2)
  var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
  var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;

  // (3)
  var clientTop = docElem.clientTop || body.clientTop || 0;
  var clientLeft = docElem.clientLeft || body.clientLeft || 0;

  // (4)
  var top  = box.top +  scrollTop - clientTop;
  var left = box.left + scrollLeft - clientLeft;

  return {
    top: Math.round(top),
    left: Math.round(left),
    height: box.height,
    bottom: box.bottom,
    screenTop: box.top
  };
}

function removeColor(css){
  return css.replace(/color:((?:(?!;).)*);/g,"");
}

function escapeHtml(text) {
  //text = $("<div />").html(text).text();
  return text
    .replace(/</g, "&lt;")
    .replace(/>/g, "&gt;");
}

function unEscapeHtml(text) {
  return text
    .replace(/&lt;/g, "<")
    .replace(/&gt;/g, ">");
}


RegExp.escapePattern = function(s) {
  return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
};

RegExp.escapeReplace = function(s) {
  return s.replace(/\$/g, "$$");
};

function replaceAll(str, find, replaceBy) {
  // console.log("str");
  // console.log(str);
  // console.log("find: " + find);
  // console.log("replaceBy: " + replaceBy);

  find = RegExp.escapePattern(find);
  replaceBy = RegExp.escapeReplace(replaceBy);
  var regex = new RegExp("(^|(\\s|<br>|<div><br></div>))" + find + "(?=(\\s|<br>|<div><br></div>|\\.|,|\\-|\\?|:|;)|$)", "g");

  var res = str.replace(regex, "$1" + replaceBy);
  return res;
}


// run  ./node_modules/.bin/mocha -R xunit tests/
// Are we in a browser or doing testing?
if (typeof(window) === "undefined") {
  exports.replaceAll = replaceAll;
  exports.uniqueArray = uniqueArray;
  exports.backtrailingWhitespace = backtrailingWhitespace;
  exports.getTextNl = getTextNl_test;
}


/**
 * Returns the style for a node.
 *
 * @param n The node to check.
 * @param p The property to retrieve (usually 'display').
 * @link http://www.quirksmode.org/dom/getstyles.html
 */
getStyle = function( n, p ) {
  return n.currentStyle ?
    n.currentStyle[p] :
    document.defaultView.getComputedStyle(n, null).getPropertyValue(p);
}

/**
 * Converts HTML to text, preserving semantic newlines for block-level
 * elements.
 *
 * @param node - The HTML node to perform text extraction.
 */
toText = function( node ) {
  var result = '';

  if( node.nodeType == document.TEXT_NODE ) {
    // Replace repeated spaces, newlines, and tabs with a single space.
    result = node.nodeValue.replace( /\s+/g, ' ' );
  }
  else {
    for( var i = 0, j = node.childNodes.length; i < j; i++ ) {
      result += toText( node.childNodes[i] );
    }

    var d = getStyle( node, 'display' );

    if( d.match( /^block/ ) || d.match( /list/ ) || d.match( /row/ ) ||
        node.tagName == 'BR' || node.tagName == 'HR' ) {
      result += '\n';
    }
  }

  return result;
}


function placeCaretAtEnd(el) {
  el.focus();
  if (typeof window.getSelection != "undefined"
          && typeof document.createRange != "undefined") {
      var range = document.createRange();
      range.selectNodeContents(el);
      range.collapse(false);
      var sel = window.getSelection();
      sel.removeAllRanges();
      sel.addRange(range);
  } else if (typeof document.body.createTextRange != "undefined") {
      var textRange = document.body.createTextRange();
      textRange.moveToElementText(el);
      textRange.collapse(false);
      textRange.select();
  }
}